package routes

import (
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	_ "github.com/mattn/go-sqlite3"
	"goDAV/DAV"
	"log"
	"net/http"
	"strings"
)


var (
	loginTries = 0
	loginCount = map[string]int{}
	
	rndSalt = "kE3p,<"
)

func WebDAVHandler(w http.ResponseWriter, r *http.Request) {
	loginCount[r.RemoteAddr] = loginTries
	w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
	b64encodedCredz := strings.Split( r.Header.Get("Authorization"), " ")
	if loginCount[r.Host] == 3{
		http.Error(w, "Not authorized", 401)
		return
	}

	if len(b64encodedCredz) != 2 {
		loginTries += 1
		http.Error(w, "Not authorized", 401)
		return
	}
	encodedCredz, err := base64.StdEncoding.DecodeString(b64encodedCredz[1])
	if err != nil{
		loginTries += 1
		http.Error(w, err.Error(), 401)
		return
	}
	creds := strings.SplitN(string(encodedCredz),":", 2)
	if len(creds)!=2{
		loginTries += 1
		http.Error(w, "Not authorized", 401)
		return
	}
	if cap(creds) == 2{
		check := CheckCredential(creds[0], creds[1])
		if !check {
			loginTries += 1
			http.Error(w, "Not authorized", 401)
			return
		}
		dav := DAV.WebDAV()
		dav.ServeHTTP(w,r)
		loginTries = 0
	}
}


func CheckCredential(username, password string) bool{
	var DBUsername string
	var DBPassword string
	dbFile := "./Database/webdav"
	db, err := sql.Open("sqlite3", dbFile)
	if err != nil{
		log.Fatal("[WEBDAVHANDLER.GO] ", err)
	}
	rows, err := db.Query("select Username, Password from User where Username = '"+username+"'")
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		err := rows.Scan(&DBUsername, &DBPassword)
		log.Println(err)
	}

	if (username == DBUsername) && (HashPassword(password) == DBPassword){
		return true
	}
	return false
}

func HashPassword(pw string) string{
	hash := sha256.New()
	hash.Write([]uint8(pw+rndSalt))
	hashed := base64.URLEncoding.EncodeToString(hash.Sum(nil))
	return hashed
}
