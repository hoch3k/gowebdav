package rest

import (
	"crypto/tls"
	"github.com/spf13/viper"
	"goDAV/DAV"
	"goDAV/rest/routes"
	"log"
	"net/http"
	"time"
)

func Start(){
	log.Println("Server listen at ", viper.GetString("ServerConf.addr"))
	mux := http.NewServeMux()
	if err := server(mux).ListenAndServeTLS(viper.GetString("ServerConf.crt"), viper.GetString("ServerConf.key")); err != nil{
		log.Fatal("[SERVER.GO], ", err)
	}
}

func server(handler *http.ServeMux) *http.Server {
	handler.HandleFunc("/", routes.Test)
	handler.HandleFunc(DAV.WebDAV().Prefix+"/", routes.WebDAVHandler)
	tlsConf := &tls.Config{
		PreferServerCipherSuites: true,
		CurvePreferences: []tls.CurveID{
			tls.CurveP256,
			tls.X25519,
		},
		MinVersion: tls.VersionTLS12,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		},
	}
	srv := &http.Server{
		Addr:              viper.GetString("ServerConf.addr"),
		Handler:           handler,
		TLSConfig:         tlsConf,
		ReadTimeout:       10 * time.Second,
		ReadHeaderTimeout: 10 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       180 * time.Second,
		MaxHeaderBytes:    0,
		TLSNextProto:      nil,
		ConnState:         nil,
		ErrorLog:          nil,
	}
	return srv
}
