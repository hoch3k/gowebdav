package main

import (
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"goDAV/rest/routes"
	"golang.org/x/crypto/ssh/terminal"
	"log"
	"os"
)

func main() {
	addUserFlag := flag.String("a", "", "Add User to WebDAV")
	passwordFlag := flag.String("p", "", "Specify User password")
	flag.Parse()
	if len(*addUserFlag) == 0 {
		os.Exit(-1)
	}
	dbFile := "/path/to/database"
	db, err := sql.Open("sqlite3", dbFile)
	if err != nil {
		log.Fatal(err)
	}

	statement, err := db.Prepare("INSERT INTO User (Username, Password) VALUES(?,?);")
	if err != nil{
		log.Fatal(err)
	}
	switch len(*passwordFlag) {
	case 0:
		fmt.Print("Please enter a password: ")
		password, err := terminal.ReadPassword(0)
		if err != nil { 
			log.Fatal(err) 
		}
		if _, err := statement.Exec(*addUserFlag, routes.HashPassword(string(password))); err != nil{
			log.Fatal(err)
		}
	default:
		if _, err := statement.Exec(*addUserFlag, routes.HashPassword(*passwordFlag)); err != nil{
			log.Fatal(err)
		}
	}
}
