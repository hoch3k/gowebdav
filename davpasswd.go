package main

import (
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"github.com/spf13/viper"
	"goDAV/rest/routes"
	"golang.org/x/crypto/ssh/terminal"
	"log"
	"os"
)



func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath("/home/bho/go/src/goDAV/config/")
	if err := viper.ReadInConfig(); err != nil{
		log.Fatal("[MAIN.GO] Error WebDAV-Handler failed to read config", err)
	}

	selectUserFlag := flag.String("u", "", "Select user to modify password")
	oldPasswordFlag := flag.String("p", "", "Enter old user password")
	newPasswordFlag := flag.String("P", "", "Specify User password")
	flag.Parse()
	if len(*selectUserFlag) == 0{
		fmt.Println("You have to enter one User to change the password")
		os.Exit(-1)
	}

	// check if the user enter the old password as argument
	switch len(*oldPasswordFlag) {
	//asking the current password before changing it
	case 0:
		fmt.Print("Please enter the old password: ")
		password, err := terminal.ReadPassword(0)
		if err != nil{
			log.Fatal(err)
		}
		fmt.Print("\nNow please enter a new password:")
		newPassword, err := terminal.ReadPassword(0)
		if err != nil{
			log.Fatal(err)
		}
		updatePassword(*selectUserFlag, string(password), string(newPassword))
	// old password was entered as argument
	default:
		updatePassword(*selectUserFlag, *oldPasswordFlag, *newPasswordFlag)
	}
}

func updatePassword(username,oldPassword, newPassword string){
	db, err := sql.Open("sqlite3", viper.GetString("WebDAV.Database"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	switch routes.CheckCredential(username, oldPassword) {
	case true:
		_, err = db.Exec("UPDATE User SET Password='" + routes.HashPassword(newPassword) + "' WHERE Username='" + username + "';")
		if err != nil{
			log.Fatal(err)
		}
	case false:
		log.Fatal("You entered a wrong password. This action has been logged")
	}
}
