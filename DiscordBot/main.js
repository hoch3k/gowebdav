const discord = require('discord.js');
const {exec} = require('child_process')

const client = new discord.Client();

// discord-bot token
const token = '';

const channelID = '';

client.on('ready', () => {
    console.log(`[*] Start: ${client.user.tag}`);
    const channel = client.channels.cache.get(channelID)
    const WelcomeMsg = new discord.MessageEmbed()
        .setTitle("Hallo Freunde")
        .setDescription('Auf diesem Server kann man sich einen Account fuer einen WebDAV-Server erstellen.\n' +
            'Diesen File-Server kann man dann auf dem eigenen Computer als Festplatte mounten.\n' +
            'Falls man beim Einbinden des Servers Hilfe braucht, \nhabe ich hier eine gute [Webseite](https://lmgtfy.com/?q=webdav+unter+windows+10+einbinden)\n'+
            'Um den Account zu erstellen einfach den Bot anschreiben.');
    channel.send(WelcomeMsg)
})

client.on('message',message => {
    switch (message.content.split(' ')[0]) {
        case 'adduser':
            // message.author.send('adding user ', message.author.username);
            let password = adduser(message.author.username);
            const embedSuccess = new discord.MessageEmbed()
                .setTitle(`User: ${message.author.username} was created`)
                .addField('Password:', password)
                .setColor('#007bff');
            message.author.send(embedSuccess);
            break;
        case 'passwd':
            message.author.send('regenerate password');
            const oldPassword = message.content.split(' ')[1]
            const passwrd = passwd(message.author.username, oldPassword)
            const embedPwChange = new discord.MessageEmbed()
                .setTitle(`Password was changed`)
                .addField('New password', passwrd)
                .setColor('#0074ff')
            message.author.send(embedPwChange)
            break;
        default:
            const embedHelp = new discord.MessageEmbed()
                .setTitle("Man page")
                .setColor("#09cb61")
                .addFields(
                    {name: 'How to get access to WebDAV-Wastelife', value: '$ adduser'},
                    {name: 'If you have forgotten your password, you can regenerate it here', value: '$ passwd'},
                    {name: 'Connection information', value: 'Host: https://xxx.xxx.xxx.xxx/ \nYou have to mount it on your local machine'}
                )
            message.author.send(embedHelp);
            break;
    }
})

function adduser(username) {
    const password = genPassword();
    exec(`/home/bho/go/src/webdav_discord_bot_js/davuseradd -a ${username} -p ${password}`, (err, stdout, stderr) => {
        if (err){
            console.log(err.message)
        }
        if (stderr) console.log(stderr);
        console.log(stdout)
    })
    return password
}

function passwd(username, oldPassword) {
    const password = genPassword();
    exec(`/home/bho/go/src/webdav_discord_bot_js/passwd -u ${username} -p ${oldPassword} -P ${password}`, (err, stdout, stderr)=>{
        if (err) console.log(err.message)
        if (stderr) console.log(stderr)
        console.log(stdout)
    })
    return password
}

function genPassword() {
    let password = '';
    let characters = 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890';
    let length = characters.length
    for (let i = 0; i < 22; i++) {
        password += characters.charAt(Math.floor(Math.random() * length))
    }
    return password
}

client.login(token)
