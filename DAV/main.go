package DAV

import (
	"github.com/spf13/viper"
	"golang.org/x/net/webdav"
	"log"
	"net/http"
)



func WebDAV() *webdav.Handler {
	webDavServer := &webdav.Handler{
		Prefix:     viper.GetString("WebDAV.Prefix"),
		FileSystem: webdav.Dir(viper.GetString("WebDAV.Directory")),
		LockSystem: webdav.NewMemLS(),
		Logger:     logger,
	}

	return webDavServer
}

func logger(r *http.Request, err error){
	f, _ := os.OpenFile("webdav.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	defer f.Close()
	if err != nil{
		logger := log.New(f, "[!] ", log.LstdFlags)
		logger.Printf("Addr: %s | Method: %s | Dest: %s\n\t\t\tError-Message: %s", r.RemoteAddr, r.Method, r.URL, err.Error())
	}else {
		logger := log.New(f, "[*] ", log.LstdFlags)
		logger.Printf("Addr: %s | Method: %s | Dest: %s\n", r.RemoteAddr, r.Method, r.URL)
	}
}

