package main

import (
	"github.com/spf13/viper"
	"goDAV/rest"
	"log"
)

func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath("/home/bho/go/src/goDAV/config/")
	if err := viper.ReadInConfig(); err != nil{
		log.Fatal("[MAIN.GO] Error WebDAV-Handler failed to read config", err)
	}
	rest.Start()


}

